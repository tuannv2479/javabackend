package com.example.demo.controller;

import com.example.demo.models.User;
import com.example.demo.pojo.Book;
import com.example.demo.repositories.UserRepository;
import javafx.scene.canvas.GraphicsContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

	@GetMapping("/sum")
	Integer sum(@RequestParam("a") Integer one,
					   @RequestParam("b") Integer two) {
		return one+two;
	}
	@PostMapping("/save-book")
	String saveBook(@RequestBody Book book){
		return book.getName();
	}

	@GetMapping("/tinh-giai-thua")
	Integer tinhGiaiThua(@RequestParam("a") Integer one) {
		int result = 1;

		for (int i = 1 ;i < one;i++) {
			result *= i;
		}

		return result;
	}
	@Autowired
	UserRepository userRepository;

	@PostMapping("/test")
	void test(@RequestBody User user){
		userRepository.save(user);
	}
}
