package com.example.demo.models;

import lombok.Data;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@Getter
@Setter

public class User {
    @Id
    @Generated
    private Long id;
    private String name;
    private String address;

}
